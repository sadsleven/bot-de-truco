function evaluarEnvido (){
    envidoJugador = 0;
    envidoBot = 0;
    //cartas del jugador
    //perico
    if (pericoCartas[carta1] == 2 || pericoCartas[carta2] == 2 || pericoCartas[carta3] == 2 ){
        if (envidoCartas[carta1]>envidoCartas[carta2] && envidoCartas[carta1]>envidoCartas[carta3]){
            if(envidoCartas[carta2]>=envidoCartas[carta3]) {
                envidoJugador = 30 + envidoCartas[carta2];
            }
            else {
                envidoJugador = 30 + envidoCartas[carta3];
            }
        }
        if (envidoCartas[carta2]>envidoCartas[carta1] && envidoCartas[carta2]>envidoCartas[carta3]){
            if(envidoCartas[carta1]>=envidoCartas[carta3]) {
                envidoJugador = 30 + envidoCartas[carta2];
            }
            else {
                envidoJugador = 30 + envidoCartas[carta3];
            }
        }
        if (envidoCartas[carta3]>envidoCartas[carta1] && envidoCartas[carta3]>envidoCartas[carta2]){
            if(envidoCartas[carta1]>=envidoCartas[carta2]) {
                envidoJugador = 30 + envidoCartas[carta1];
            }
            else {
                envidoJugador = 30 + envidoCartas[carta2];
            }
        }
    }
    //perica
    if (envidoJugador == 0) {
        if (pericoCartas[carta1] == 1 || pericoCartas[carta2] == 1 || pericoCartas[carta3] == 1 ){
            if (envidoCartas[carta1]>envidoCartas[carta2] && envidoCartas[carta1]>envidoCartas[carta3]){
                if(envidoCartas[carta2]>=envidoCartas[carta3]) {
                    envidoJugador = 29 + envidoCartas[carta2];
                }
                else {
                    envidoJugador = 29 + envidoCartas[carta3];
                }
            }
            if (envidoCartas[carta2]>envidoCartas[carta1] && envidoCartas[carta2]>envidoCartas[carta3]){
                if(envidoCartas[carta1]>=envidoCartas[carta3]) {
                    envidoJugador = 29 + envidoCartas[carta2];
                }
                else {
                    envidoJugador = 29 + envidoCartas[carta3];
                }
            }
            if (envidoCartas[carta3]>envidoCartas[carta1] && envidoCartas[carta3]>envidoCartas[carta2]){
                if(envidoCartas[carta1]>=envidoCartas[carta2]) {
                    envidoJugador = 29 + envidoCartas[carta1];
                }
                else {
                    envidoJugador = 29 + envidoCartas[carta2];
                }
            }
        }
    }
    // dobles sin perico ni perica
    if ( envidoJugador == 0 ){
        if (viraCartas[carta1] == viraCartas[carta2]) {
            envidoJugador = envidoCartas[carta1] + envidoCartas[carta2] + 20;
        }
        if (viraCartas[carta2] == viraCartas[carta3]) {
            envidoJugador = envidoCartas[carta2] + envidoCartas[carta3] + 20;
        }
        if (viraCartas[carta1] == viraCartas[carta3]) {
            envidoJugador = envidoCartas[carta1] + envidoCartas[carta3] + 20;
        }
    }
    // carta sola mas alta
    if ( envidoJugador == 0) {
        if (envidoCartas[carta1] >= envidoCartas[carta2] && envidoCartas[carta1] >= envidoCartas[carta2]){
            envidoJugador = envidoCartas[carta1];
        }
        if (envidoCartas[carta2] >= envidoCartas[carta1] && envidoCartas[carta2] >= envidoCartas[carta3]){
            envidoJugador = envidoCartas[carta2];
        }
        if (envidoCartas[carta3] >= envidoCartas[carta1] && envidoCartas[carta3] >= envidoCartas[carta2]){
            envidoJugador = envidoCartas[carta3];
        }
    }
    // cartas del bot 
    //perico
    if (pericoCartas[cartabot1] == 2 || pericoCartas[cartabot2] == 2 || pericoCartas[cartabot3] == 2 ){
        if (envidoCartas[cartabot1]>envidoCartas[cartabot2] && envidoCartas[cartabot1]>envidoCartas[cartabot3]){
            if(envidoCartas[cartabot2]>=envidoCartas[cartabot3]) {
                envidoBot = 30 + envidoCartas[cartabot2];
            }
            else {
                envidoBot = 30 + envidoCartas[cartabot3];
            }
        }
        if (envidoCartas[cartabot2]>envidoCartas[cartabot1] && envidoCartas[cartabot2]>envidoCartas[cartabot3]){
            if(envidoCartas[cartabot1]>=envidoCartas[cartabot3]) {
                envidoBot = 30 + envidoCartas[cartabot2];
            }
            else {
                envidoBot = 30 + envidoCartas[cartabot3];
            }
        }
        if (envidoCartas[cartabot3]>envidoCartas[cartabot1] && envidoCartas[cartabot3]>envidoCartas[cartabot2]){
            if(envidoCartas[cartabot1]>=envidoCartas[cartabot2]) {
                envidoBot = 30 + envidoCartas[cartabot1];
            }
            else {
                envidoBot = 30 + envidoCartas[cartabot2];
            }
        }
    }
    //perica
    if (envidoBot == 0) {
        if (pericoCartas[cartabot1] == 1 || pericoCartas[cartabot2] == 1 || pericoCartas[cartabot3] == 1 ){
            if (envidoCartas[cartabot1]>envidoCartas[cartabot2] && envidoCartas[cartabot1]>envidoCartas[cartabot3]){
                if(envidoCartas[cartabot2]>=envidoCartas[cartabot3]) {
                    envidoBot = 29 + envidoCartas[cartabot2];
                }
                else {
                    envidoBot = 29 + envidoCartas[cartabot3];
                }
            }
            if (envidoCartas[cartabot2]>envidoCartas[cartabot1] && envidoCartas[cartabot2]>envidoCartas[cartabot3]){
                if(envidoCartas[cartabot1]>=envidoCartas[cartabot3]) {
                    envidoBot = 29 + envidoCartas[cartabot2];
                }
                else {
                    envidoBot = 29 + envidoCartas[cartabot3];
                }
            }
            if (envidoCartas[cartabot3]>envidoCartas[cartabot1] && envidoCartas[cartabot3]>envidoCartas[cartabot2]){
                if(envidoCartas[cartabot1]>=envidoCartas[cartabot2]) {
                    envidoBot = 29 + envidoCartas[cartabot1];
                }
                else {
                    envidoBot = 29 + envidoCartas[cartabot2];
                }
            }
        }
    }
    // dobles sin perico o perica
    if (envidoBot == 0) {
        if (viraCartas[cartabot1] == viraCartas[cartabot2]) {
            envidoBot = envidoCartas[cartabot1] + envidoCartas[cartabot2] + 20;
        }
        if (viraCartas[cartabot2] == viraCartas[cartabot3]) {
            envidoBot = envidoCartas[cartabot2] + envidoCartas[cartabot3] + 20;
        }
        if (viraCartas[cartabot1] == viraCartas[cartabot3]) {
            envidoBot = envidoCartas[cartabot1] + envidoCartas[cartabot3] + 20;
        }
    }
    // carta sola mas alta
    if (envidoBot == 0) {
        if (envidoCartas[cartabot1] >= envidoCartas[cartabot2] && envidoCartas[cartabot1] >= envidoCartas[cartabot3]){
            envidoBot = envidoCartas[cartabot1];
        }
        if (envidoCartas[cartabot2] >= envidoCartas[cartabot3] && envidoCartas[cartabot2] >= envidoCartas[cartabot3]){
            envidoBot = envidoCartas[cartabot2];
        }
        if (envidoCartas[cartabot3] >= envidoCartas[cartabot1] && envidoCartas[cartabot3] >= envidoCartas[cartabot2]){
            envidoBot = envidoCartas[cartabot3];
        }
    }
}
function evaluarFlor (){
    florJugador = 0;
    florBot = 0
    //flor del jugador
    //flor reserveada
    if (pericoCartas[carta1] == 2 || pericoCartas[carta2] == 2 || pericoCartas[carta3] == 2){
        if (pericoCartas[carta1] == 1 || pericoCartas[carta2] == 1 || pericoCartas[carta3] == 1){
            if (pericoCartas[carta1] == 0) florJugador = 59 + envidoCartas[carta1];
            if (pericoCartas[carta2] == 0) florJugador = 59 + envidoCartas[carta2];
            if (pericoCartas[carta3] == 0) florJugador = 59 + envidoCartas[carta3];
        }
    }
    //flor perico
    if (pericoCartas[carta1] == 2) {
        if (viraCartas[carta2] == viraCartas[carta3]) florJugador = 30 + envidoCartas[carta2] + envidoCartas[carta3];
    }
    if (pericoCartas[carta2] == 2) {
        if (viraCartas[carta1] == viraCartas[carta3]) florJugador = 30 + envidoCartas[carta1] + envidoCartas[carta3];
    }
    if (pericoCartas[carta3] == 2) {
        if (viraCartas[carta1] == viraCartas[carta2]) florJugador = 30 + envidoCartas[carta1] + envidoCartas[carta2];
    }
    //flor perica
    if (pericoCartas[carta1] == 1) {
        if (viraCartas[carta2] == viraCartas[carta3]) florJugador = 29 + envidoCartas[carta2] + envidoCartas[carta3];
    }
    if (pericoCartas[carta2] == 1) {
        if (viraCartas[carta1] == viraCartas[carta3]) florJugador = 29 + envidoCartas[carta1] + envidoCartas[carta3];
    }
    if (pericoCartas[carta3] == 1) {
        if (viraCartas[carta1] == viraCartas[carta2]) florJugador = 29 + envidoCartas[carta1] + envidoCartas[carta2];
    }
    //flor normal   
    if (viraCartas[carta1] == viraCartas[carta2] && viraCartas[carta1] == viraCartas[carta3]){
        florJugador = envidoCartas[carta1] + envidoCartas[carta2] + envidoCartas[carta3] + 20;
    }
    //flor del bot
    //flor reserveada
    if (pericoCartas[cartabot1] == 2 || pericoCartas[cartabot2] == 2 || pericoCartas[cartabot3] == 2 ) { 
        if (pericoCartas[cartabot1] == 1 || pericoCartas[cartabot2] == 1 || pericoCartas[cartabot3] == 1) {
            if (pericoCartas[cartabot1] == 0) florBot = 59 + envidoCartas[cartabot1];
            if (pericoCartas[cartabot2] == 0) florBot = 59 + envidoCartas[cartabot2];
            if (pericoCartas[cartabot3] == 0) florBot = 59 + envidoCartas[cartabot3];
        }
    }
    //flor perico
    if (pericoCartas[cartabot1] == 2) {
        if (viraCartas[cartabot2] == viraCartas[cartabot3]) florBot = 30 + envidoCartas[cartabot2] + envidoCartas[cartabot3];
    }
    if (pericoCartas[cartabot2] == 2) {
        if (viraCartas[cartabot1] == viraCartas[cartabot3]) florBot = 30 + envidoCartas[cartabot1] + envidoCartas[cartabot3];
    }
    if (pericoCartas[cartabot3] == 2) {         
        if (viraCartas[cartabot1] == viraCartas[cartabot2]) florBot = 30 + envidoCartas[cartabot1] + envidoCartas[cartabot2];
    }
    //flor perica
    if (pericoCartas[cartabot1] == 1) {
        if (viraCartas[cartabot2] == viraCartas[cartabot3]) florBot = 29 + envidoCartas[cartabot2] + envidoCartas[cartabot3];
    }
    if (pericoCartas[cartabot2] == 1) {
        if (viraCartas[cartabot1] == viraCartas[cartabot3]) florBot = 29 + envidoCartas[cartabot1] + envidoCartas[cartabot3];
    }
    if (pericoCartas[cartabot3] == 1) {
        if (viraCartas[cartabot1] == viraCartas[cartabot2]) florBot = 29 + envidoCartas[cartabot1] + envidoCartas[cartabot2];
    }
    //flor normal   
    if (viraCartas[cartabot1] == viraCartas[cartabot2] && viraCartas[cartabot1] == viraCartas[cartabot3]){
        florBot = envidoCartas[cartabot1] + envidoCartas[cartabot2] + envidoCartas[cartabot3] + 20;
    }
}
function valorActual(){
    valorCartas = cartas.map((carta)=>carta.valor);
    envidoCartas = cartas.map((carta)=>carta.envido);
    pericoCartas = cartas.map((carta)=>carta.perico);
    //espada
    if (vira == 0 || vira == 1 || vira == 2 || vira == 3 || vira == 4 || vira == 5 || vira == 6 || vira == 9){
        valorCartas[7] = 14;
        valorCartas[8] = 15;
        envidoCartas[7] = 9;
        envidoCartas[8] = 10;
        pericoCartas[7] = 1;
        pericoCartas[8] = 2;
    }
    if (vira == 7) {
        valorCartas[8] = 15;
        valorCartas[9] = 14;
        envidoCartas[8] = 10;
        envidoCartas[9] = 9;
        pericoCartas[8] = 2;
        pericoCartas[9] = 1;
    }
    if (vira == 8) {
        valorCartas[7] = 14;
        valorCartas[9] = 15;
        envidoCartas[7] = 9;
        envidoCartas[9] = 10;
        pericoCartas[7] = 1;
        pericoCartas[9] = 2;
    }
    //basto
    if (vira == 10 || vira == 11 || vira == 12 || vira == 13 || vira == 14 || vira == 15 || vira == 16 || vira == 19){
        valorCartas[17] = 14;
        valorCartas[18] = 15;
        envidoCartas[17] = 9;
        envidoCartas[18] = 10;
        pericoCartas[17] = 1;
        pericoCartas[18] = 2;
    }
    if (vira == 17) {
        valorCartas[18] = 15;
        valorCartas[19] = 14;
        envidoCartas[18] = 10;
        envidoCartas[19] = 9;
        pericoCartas[18] = 2;
        pericoCartas[19] = 1;
    }
    if (vira == 18) {
        valorCartas[17] = 14;
        valorCartas[19] = 15;
        envidoCartas[17] = 9;
        envidoCartas[19] = 10;
        pericoCartas[17] = 1;
        pericoCartas[19] = 2;
    }
    //oro
    if (vira == 20 || vira == 21 || vira == 22 || vira == 23 || vira == 24 || vira == 25 || vira == 26 || vira == 29){
        valorCartas[27] = 14;
        valorCartas[28] = 15;
        envidoCartas[27] = 9;
        envidoCartas[28] = 10;
        pericoCartas[27] = 1;
        pericoCartas[28] = 2;
    }
    if (vira == 27) {
        valorCartas[28] = 15;
        valorCartas[29] = 14;
        envidoCartas[28] = 10;
        envidoCartas[29] = 9;
        pericoCartas[28] = 2;
        pericoCartas[29] = 1;
    }
    if (vira == 28) {
        valorCartas[27] = 14;
        valorCartas[29] = 15;
        envidoCartas[27] = 9;
        envidoCartas[29] = 10;
        pericoCartas[27] = 1;
        pericoCartas[29] = 2;
    }
    //copa
    if (vira == 30 || vira == 31 || vira == 32 || vira == 33 || vira == 34 || vira == 35 || vira == 36 || vira == 39){
        valorCartas[37] = 14;
        valorCartas[38] = 15;
        envidoCartas[37] = 9;
        envidoCartas[38] = 10;
        pericoCartas[37] = 1;
        pericoCartas[38] = 2;
    }
    if (vira == 37) {
        valorCartas[38] = 15;
        valorCartas[39] = 14;
        envidoCartas[38] = 10;
        envidoCartas[39] = 9;
        pericoCartas[38] = 2;
        pericoCartas[39] = 1;
    }
    if (vira == 38) {
        valorCartas[37] = 14;
        valorCartas[39] = 15;
        envidoCartas[37] = 9;
        envidoCartas[39] = 10;
        pericoCartas[37] = 1;
        pericoCartas[39] = 2;
    }
}