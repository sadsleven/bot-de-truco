const cartas = [
    {posicion : 'imagenes/espada1.jpg', valor : 13 , envido : 1 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada2.jpg', valor : 8 , envido : 2 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada3.jpg', valor : 9 , envido : 3 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada4.jpg', valor : 0 , envido : 4 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada5.jpg', valor : 1 , envido : 5 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada6.jpg', valor : 2 , envido : 6 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada7.jpg', valor : 11 , envido : 7 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada10.jpg', valor : 4 , envido : 0 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada11.jpg', valor : 5 , envido : 0 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/espada12.jpg', valor : 6 , envido : 0 , vira : 0 , perico : 0 },
    {posicion : 'imagenes/basto1.jpg', valor : 12 , envido : 1 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto2.jpg', valor : 8 , envido : 2 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto3.jpg', valor : 9 , envido : 3 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto4.jpg', valor : 0 , envido : 4 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto5.jpg', valor : 1 , envido : 5 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto6.jpg', valor : 2 , envido : 6 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto7.jpg', valor : 3 , envido : 7 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto10.jpg', valor : 4 , envido : 0 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto11.jpg', valor : 5 , envido : 0 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/basto12.jpg', valor : 6 , envido : 0 , vira : 1 , perico : 0 },
    {posicion : 'imagenes/oro1.jpg', valor : 7 , envido : 1 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro2.jpg', valor : 8 , envido : 2 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro3.jpg', valor : 9 , envido : 3 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro4.jpg', valor : 0 , envido : 4 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro5.jpg', valor : 1 , envido : 5 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro6.jpg', valor : 2 , envido : 6 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro7.jpg', valor : 10 , envido : 7 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro10.jpg', valor : 4 , envido : 0 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro11.jpg', valor : 5 , envido : 0 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/oro12.jpg', valor : 6 , envido : 0 , vira : 2 , perico : 0 },
    {posicion : 'imagenes/copa1.jpg', valor : 7 , envido : 1 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa2.jpg', valor : 8 , envido : 2 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa3.jpg', valor : 9 , envido : 3 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa4.jpg', valor : 0 , envido : 4 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa5.jpg', valor : 1 , envido : 5 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa6.jpg', valor : 2 , envido : 6 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa7.jpg', valor : 3 , envido : 7 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa10.jpg', valor : 4 , envido : 0 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa11.jpg', valor : 5 , envido : 0 , vira : 3 , perico : 0 },
    {posicion : 'imagenes/copa12.jpg', valor : 6 , envido : 0 , vira : 3 , perico : 0 }
]
let carta1, cartabot1, carta2, cartabot2, botcarta3, cartabot3, vira, envidoJugador, envidoBot;
let florJugador, florBot, piedrasJugador, piedrasBot, turnoJugar, cartamesaJugador1, cartamesaJugador2;
let cartamesaJugador3, cartamesaBot1, cartamesaBot2, cartamesaBot3, empardo, poderBarajar = false, jugarTruco = false;
let ganadorTruco =0 , picardia = 0, truco, retruco, valenueve, valejuego, envidoJuego, puntosEnvido, florJuego, puntosFlor;
let css="100%", igual="-100%", esperandoRespuesta=false, findejuego = false, valorcartasBot, botquiso, resto, botquisoenvido;
let vozenvido, ganadorEnvido, ganadorFlor, vozFlor;
let ubicacionCartas = cartas.map((carta)=>carta.posicion);
let valorCartas = cartas.map((carta)=>carta.valor);
let envidoCartas = cartas.map((carta)=>carta.envido);
let viraCartas = cartas.map((carta)=>carta.vira);
let pericoCartas = cartas.map((carta)=>carta.perico);

function juegoTruco(){
    if (jugarTruco == true){
        limpiarMesa();
        piedrasJugador = 0;
        piedrasBot = 0;
        turnoJugar = Math.round(Math.random());
        // cero es jugador y 1 es bot
        if (turnoJugar== 0) console.log("Juega el jugador");
        if (turnoJugar== 1) console.log("Juega el bot");
        if (turnoJugar== 1) document.getElementById("tocabarajar").innerHTML="Te toca barajar";
        poderBarajar = true;
        if (turnoJugar == 0) barajarCartas();
        jugarTruco = false;
        document.getElementById("piedrasbot").innerHTML=`Piedras del bot: ${piedrasBot}`;
        document.getElementById("piedrasjug").innerHTML=`Piedras del jugador: ${piedrasJugador}`;
    }
}
function terminarTruco(){
    document.getElementById("ganadorjuego").innerHTML=" ";
    document.getElementById("tocabarajar").innerHTML=" ";
    limpiarMesa();
    jugarTruco = true;
    document.getElementById("aceptarboton").style.left=igual;
}
function barajarCartas (){
    if (poderBarajar == true){
        document.getElementById("ganadorjuego").innerHTML=" ";
        document.getElementById("ganadorenvido").innerHTML=" ";
        document.getElementById("tocabarajar").innerHTML=" ";
        limpiarMesa ();
        if (turnoJugar== 0) console.log("Juega el jugador");
        if (turnoJugar== 1) console.log("Juega el bot");
        ganadorTruco = 0;
        // 1 es jugador y 2 es bot
        empardo = false;
        do {
            carta1 = Math.round(Math.random()*39);
            cartabot1 = Math.round(Math.random()*39);
            carta2 = Math.round(Math.random()*39);
            cartabot2 = Math.round(Math.random()*39);
            carta3 = Math.round(Math.random()*39);
            cartabot3 = Math.round(Math.random()*39);
            vira = Math.round(Math.random()*39);
        } while (carta1 == cartabot1 || carta1 == carta2 || carta1 == cartabot2 || carta1 == carta3 
        || carta1 == cartabot3 || carta1 == vira ||  cartabot1 == carta2 || cartabot1 == cartabot2 || 
        cartabot1 == carta3  || cartabot1 == cartabot3 || cartabot1 == vira || carta2 == cartabot2 || 
        carta2 == carta3 || carta2 == cartabot3 || carta2 == vira || cartabot2 == carta3 || 
        cartabot2 == cartabot3 || cartabot2 == vira ||  carta3 == cartabot3 || carta3 == vira || cartabot3 == vira);

        document.getElementById("cartamano1").src = ubicacionCartas[carta1];
        document.getElementById("cartamano2").src = ubicacionCartas[carta2];
        document.getElementById("cartamano3").src = ubicacionCartas[carta3];
        //document.getElementById("carta4").src = ubicacionCartas[cartabot1];
        //document.getElementById("carta5").src = ubicacionCartas[cartabot2];
        //document.getElementById("carta6").src = ubicacionCartas[cartabot3];
        document.getElementById("vira").src = ubicacionCartas[vira];
        
        valorActual();
        evaluarEnvido();
        evaluarFlor();
        valorcartasBot = valorCartas[cartabot1] + valorCartas[cartabot2] + valorCartas[cartabot3];

        console.log(`Envido del jugador: ${envidoJugador}`);
        console.log(`Envido del bot: ${envidoBot}`);
        console.log(`Flor del jugador: ${florJugador}`);
        console.log(`Flor del bot: ${florBot}`);
        console.log(`Carta del bot 1: ${ubicacionCartas[cartabot1]}`);
        console.log(`Carta del bot 2: ${ubicacionCartas[cartabot2]}`);
        console.log(`Carta del bot 3: ${ubicacionCartas[cartabot3]}`);
        console.log(valorcartasBot);
        poderBarajar = false;
        if (turnoJugar == 1) desicionBotcarta1();
        esperandoRespuesta = true;
        voz=true;
        vozenvido = true;
        vozflor = true;
        vozenvidoflor = false;
    }
}

function jugarCarta1 (){
    limpiarlog();
    evaluarEmpardo ();
    while(carta1!= -1){
        if (esperandoRespuesta == false) break;
        // si juega primero
        if (turnoJugar == 0 && cartamesaJugador1 == -1) {
            cartamesaJugador1 = carta1;
            document.getElementById("carta1").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            vozenvido = false;
            desicionBotcarta1();
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaBot1]>valorCartas[cartamesaJugador1]){
                desisionBotcarta2();
            }
            break;
        }
        // si el bot no mato primera y no fue empardo
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == false && cartamesaBot1 != -1 
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] ){
            cartamesaJugador2 = carta1;
            document.getElementById("carta2").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            if (cartamesaJugador2 != -1) desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si el bot mato primera y hay q jugar segunda
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 == -1){
            cartamesaJugador2 = carta1;
            document.getElementById("carta2").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            evaluarJugada();
            break;
        }
        // si el bot mato primera y uno jugo ya segunda y mato
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 != -1){
            cartamesaJugador3 = carta1;
            document.getElementById("carta3").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            desisionBotcarta3();
            evaluarJugada();
            break;
        }
        // si el bot empardo primera
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == true){
            cartamesaJugador3 = carta1;
            document.getElementById("carta3").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            if (carta2 == -1){
                cartamesaJugador2 = carta3;
                document.getElementById("cartamano3").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta3= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            if (carta3 == -1){
                cartamesaJugador2 = carta2;
                document.getElementById("cartamano2").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta2= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la primera
        if (turnoJugar == 1 && cartamesaJugador1 == -1 && cartamesaBot1 != -1) {
            cartamesaJugador1 = carta1;
            document.getElementById("carta1").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            vozenvido = false;
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1]){
                desisionBotcarta2();
            }
            if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1]){
                desisionBotempardo();
            }
            break;
        }
        // si empardamos primera
        if (turnoJugar == 1 && empardo == true){
            cartamesaJugador3 = carta1;
            document.getElementById("carta3").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            if (carta2 == -1){
                cartamesaJugador2 = carta3;
                document.getElementById("cartamano3").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta3= -1;
                evaluarJugada();
                break;
            }
            if (carta3 == -1){
                cartamesaJugador2 = carta2;
                document.getElementById("cartamano2").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta2= -1;
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de ganar la primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta1;
            document.getElementById("carta2").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1
            desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de no haber matado la primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta1;
            document.getElementById("carta2").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1
            evaluarJugada();
            break;
        }
        // si uno juega segundo y nos toca echar la tercera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaJugador2 != -1 && cartamesaBot2 != -1) {
            cartamesaJugador3 = carta1;
            document.getElementById("carta3").src = ubicacionCartas[carta1];
            document.getElementById("cartamano1").src ="imagenes/fondomano.png";
            carta1= -1;
            evaluarJugada();
            if (cartamesaBot3 == -1){
                desisionBotcarta3();
                evaluarJugada();
            }
            break;
        }
        break;
    }
    evaluarEmpardo ();
    document.getElementById("truco").innerHTML="";
}

function jugarCarta2 (){
    limpiarlog();
    evaluarEmpardo ();
    while (carta2 != -1){
        if (esperandoRespuesta == false) break;
        // si juega primero
        if (turnoJugar == 0 && cartamesaJugador1 == -1) {
            cartamesaJugador1 = carta2;
            document.getElementById("carta1").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            vozenvido = false;
            desicionBotcarta1();
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaBot1]>valorCartas[cartamesaJugador1]){
                desisionBotcarta2();
            }
            break;
        }
        // si el bot no mato primera y no fue empardo
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == false && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] ){
            cartamesaJugador2 = carta2;
            document.getElementById("carta2").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            if (cartamesaJugador2 != -1) desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si el bot mato primera y hay q jugar segunda
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 == -1){
            cartamesaJugador2 = carta2;
            document.getElementById("carta2").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            evaluarJugada();
            break;
        }
        // si el bot mato primera y uno jugo ya segunda
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 != -1){
            cartamesaJugador3 = carta2;
            document.getElementById("carta3").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            desisionBotcarta3();
            evaluarJugada();
            break;
        }
        // si el bot empardo primera
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == true){
            cartamesaJugador3 = carta2;
            document.getElementById("carta3").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes1fondomano.png";
            carta2 = -1;
            if (carta1 == -1){
                cartamesaJugador2 = carta3;
                document.getElementById("cartamano3").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta3= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            if (carta3 = -1){
                cartamesaJugador2 = carta1;
                document.getElementById("cartamano1").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta1= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la primera
        if (turnoJugar == 1 && cartamesaJugador1 == -1 && cartamesaBot1 != -1) {
            cartamesaJugador1 = carta2;
            vozenvido = false;
            document.getElementById("carta1").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1]){
                desisionBotcarta2();
            }
            if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1]){
                desisionBotempardo();
            }
            break;
        }
        // si empardamos primera
        if (turnoJugar == 1 && empardo == true){
            cartamesaJugador3 = carta2;
            document.getElementById("carta3").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            if (carta1 == -1){
                cartamesaJugador2 = carta3;
                document.getElementById("cartamano3").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta3= -1;
                evaluarJugada();
                break;
            }
            if (carta3 = -1){
                cartamesaJugador2 = carta1;
                document.getElementById("cartamano1").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta1= -1;
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de haber matado primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta2;
            document.getElementById("carta2").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de no haber matado primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta2;
            document.getElementById("carta2").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            evaluarJugada();
            break;
        }
        // si uno juega segundo y nos toca echar la tercera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaJugador2 != -1 && cartamesaBot2 != -1) {
            cartamesaJugador3 = carta2;
            document.getElementById("carta3").src = ubicacionCartas[carta2];
            document.getElementById("cartamano2").src ="imagenes/fondomano.png";
            carta2 = -1;
            evaluarJugada();
            if (cartamesaBot3 == -1){
                desisionBotcarta3();
                evaluarJugada();
            }
            break;
        }
        break;
    }
    evaluarEmpardo ();
    document.getElementById("truco").innerHTML="";
}

function jugarCarta3 (){
    limpiarlog();
    evaluarEmpardo ();
    while(carta3 != -1){
        if (esperandoRespuesta == false) break;
        // si juega primero
        if (turnoJugar == 0 && cartamesaJugador1 == -1) {
            cartamesaJugador1 = carta3;
            vozenvido = false;
            document.getElementById("carta1").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            desicionBotcarta1();
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaBot1]>valorCartas[cartamesaJugador1]){
                desisionBotcarta2();
            }
            break;
        }
        // si el bot no mato primera y no fue empardo
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == false && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] ){
            cartamesaJugador2 = carta3;
            document.getElementById("carta2").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            if (cartamesaJugador2 != -1) desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si el bot mato primera y hay q jugar segunda
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 == -1){
            cartamesaJugador2 = carta3;
            document.getElementById("carta2").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            evaluarJugada();
            break;
        }
        // si el bot mato primera y uno jugo ya segunda
        if (turnoJugar == 0 && cartamesaBot2 != -1 && cartamesaJugador2 != -1){
            cartamesaJugador3 = carta3;
            document.getElementById("carta3").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            desisionBotcarta3();
            evaluarJugada();
            break;
        }
        // si el bot empardo primera
        if (turnoJugar == 0 && cartamesaBot2 == -1 && cartamesaJugador1 != -1 && empardo == true){
            cartamesaJugador3 = carta3;
            document.getElementById("carta3").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes1fondomano.png";
            carta3 = -1;
            if (carta1 == -1){
                cartamesaJugador2 = carta2;
                document.getElementById("cartamano2").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta2= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            if (carta2 = -1){
                cartamesaJugador2 = carta1;
                document.getElementById("cartamano").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta1= -1;
                desisionBotempardo();
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la primera
        if (turnoJugar == 1 && cartamesaJugador1 == -1 && cartamesaBot1 != -1) {
            cartamesaJugador1 = carta3;
            vozenvido = false;
            document.getElementById("carta1").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            vozflor = false;
            vozenvidoflor = true;
            if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1]){
                desisionBotcarta2();
            }
            if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1]){
                desisionBotempardo();
            }
            break;
        }
        // si empardamos primera
        if (turnoJugar == 1 && empardo == true){
            cartamesaJugador3 = carta3;
            document.getElementById("carta3").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            if (carta1 == -1){
                cartamesaJugador2 = carta2;
                document.getElementById("cartamano2").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta2= -1;
                evaluarJugada();
                break;
            }
            if (carta2 = -1){
                cartamesaJugador2 = carta1;
                document.getElementById("cartamano").src ="imagenes/fondomano.png";
                document.getElementById("carta2").src ="imagenes/naipe.png";
                carta1= -1;
                evaluarJugada();
                break;
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de haber matado primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta3 ;
            document.getElementById("carta2").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            desisionBotcarta2();
            evaluarJugada();
            if (valorCartas[cartamesaBot2]>valorCartas[cartamesaJugador2]){
                desisionBotcarta3();
            }
            break;
        }
        // si uno juega segundo y nos toca echar la segunda despues de no haber matado primera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaBot1 != -1
            && valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && cartamesaJugador3 == -1
            && cartamesaJugador2 == -1) {
            cartamesaJugador2 = carta3;
            document.getElementById("carta2").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            evaluarJugada();
            break;
        }
        // si uno juega segundo y nos toca echar la tercera
        if (turnoJugar == 1 && cartamesaJugador1 != -1 && cartamesaJugador2 != -1 && cartamesaBot2 != -1) {
            cartamesaJugador3 = carta3;
            document.getElementById("carta3").src = ubicacionCartas[carta3];
            document.getElementById("cartamano3").src ="imagenes/fondomano.png";
            carta3 = -1;
            evaluarJugada();
            if (cartamesaBot3 == -1){
                desisionBotcarta3();
                evaluarJugada();
            }
            break;
        }
        break;
    }
    evaluarEmpardo ();
    document.getElementById("truco").innerHTML="";
}

function evaluarJugada (){
    if (cartamesaJugador1!=-1 && cartamesaJugador2!=-1 && cartamesaBot1!=-1 && cartamesaBot2!=-1){
        // cuando gana el jugador
        if (valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] &&
            valorCartas[cartamesaJugador2]<valorCartas[cartamesaBot2] &&
            cartamesaBot3 != -1 && cartamesaJugador3 != -1){
                ganadorTruco = 1;
        }
        if (valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]>=valorCartas[cartamesaBot2]){
                ganadorTruco = 1;
            }
        if (valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]<valorCartas[cartamesaBot2] &&
            valorCartas[cartamesaJugador3]>=valorCartas[cartamesaBot3]){ 
                ganadorTruco = 1;
            }
        if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]>valorCartas[cartamesaBot2] &&
            valorCartas[cartamesaJugador3]>valorCartas[cartamesaBot3]){ 
                ganadorTruco = 1;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]>valorCartas[cartamesaBot3]){ 
                ganadorTruco = 1;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador2]>valorCartas[cartamesaBot2]){ 
                document.getElementById("carta5").src = ubicacionCartas[cartamesaBot2];
                document.getElementById("carta2").src = ubicacionCartas[cartamesaJugador2];
                ganadorTruco = 1;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador2]==valorCartas[cartamesaBot2] &&
            turnoJugar==0){
                document.getElementById("carta5").src = ubicacionCartas[cartamesaBot2];
                document.getElementById("carta2").src = ubicacionCartas[cartamesaJugador2]; 
                ganadorTruco = 1;
        }
        //cuando gana el bot
        if (valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] &&
            valorCartas[cartamesaJugador2]>valorCartas[cartamesaBot2] &&
            cartamesaBot3 != -1 && cartamesaJugador3 != -1){
                ganadorTruco = 2;
        }
        if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]<=valorCartas[cartamesaBot2]){
                ganadorTruco = 2;
            }
        if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]>valorCartas[cartamesaBot2] &&
            valorCartas[cartamesaJugador3]<=valorCartas[cartamesaBot3]){ 
                ganadorTruco = 2;
            }
        if (valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador2]<valorCartas[cartamesaBot2] &&
            valorCartas[cartamesaJugador3]<valorCartas[cartamesaBot3]){ 
                ganadorTruco = 2;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]<valorCartas[cartamesaBot3]){ 
                ganadorTruco = 2;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador2]<valorCartas[cartamesaBot2]){
                document.getElementById("carta5").src = ubicacionCartas[cartamesaBot2];
                document.getElementById("carta2").src = ubicacionCartas[cartamesaJugador2]; 
                ganadorTruco = 2;
            }
        if (valorCartas[cartamesaJugador1]==valorCartas[cartamesaBot1] && 
            valorCartas[cartamesaJugador3]==valorCartas[cartamesaBot3] &&
            valorCartas[cartamesaJugador2]==valorCartas[cartamesaBot2] &&
            turnoJugar==1){ 
                document.getElementById("carta5").src = ubicacionCartas[cartamesaBot2];
                document.getElementById("carta2").src = ubicacionCartas[cartamesaJugador2];
                ganadorTruco = 2;
        }
    }
    if (ganadorTruco == 1){
        esperandoRespuesta=false;
        mostrarLog();
    }
    if (ganadorTruco == 2) {
        esperandoRespuesta=false;
        mostrarLog();
    }

}
function mostrarLog(){
    evaluarPuntostruco ();
    evaluarPuntosenvido();
    //evaluarPuntosflor ();
    document.getElementById("aceptarboton").style.left=css;
    if (ganadorTruco == 1) document.getElementById("ganadorjuego").innerHTML="Tu has ganado el truco";
    if (ganadorTruco == 2) document.getElementById("ganadorjuego").innerHTML="El bot ha ganado el truco";
    if (ganadorEnvido == 1) document.getElementById("ganadorenvido").innerHTML= `El jugador gano el envido con: ${envidoJugador}`;
    if (ganadorEnvido == 2) document.getElementById("ganadorenvido").innerHTML= `El bot gano el envido con: ${envidoBot}`;
    if (ganadorEnvido == 3) document.getElementById("ganadorenvido").innerHTML= `Hubo empate de envido con: ${envidoJugador}, gano el jugador por mano`;
    if (ganadorEnvido == 4) document.getElementById("ganadorenvido").innerHTML= `Hubo empate de envido con: ${envidoJugador}, gano el bot por mano`;
    if (ganadorFlor == 1) document.getElementById("ganadorflor").innerHTML= `El jugador gano la flor con: ${florJugador}`;
    if (ganadorFlor == 2) document.getElementById("ganadorflor").innerHTML= `El bot gano la flor con: ${florBot}`;
    if (ganadorFlor == 3) document.getElementById("ganadorflor").innerHTML= `Hubo empate de flor con: ${florJugador}, gano el jugador por mano`;
    if (ganadorFlor == 4) document.getElementById("ganadorflor").innerHTML= `Hubo empate de flor con: ${florJugador}, gano el bot por mano`;
    document.getElementById("piedrasbot").innerHTML=`Piedras del bot: ${piedrasBot}`;
    document.getElementById("piedrasjug").innerHTML=`Piedras del jugador: ${piedrasJugador}`;
}
function limpiarlog(){
    document.getElementById("truco").innerHTML="";
    document.getElementById("envido").innerHTML="";
    document.getElementById("flor").innerHTML="";
    document.getElementById("ganadorenvido").innerHTML="";
}
function aceptarJuego(){
    if (ganadorTruco != 0){
        ganadorTruco == 0;
        poderBarajar = true;
        while (2>1){
            if (turnoJugar==1){
                turnoJugar=0;
                barajarCartas();
                break;
            }
            if (turnoJugar==0) {
                turnoJugar = 1;
                document.getElementById("ganadorjuego").innerHTML="Te toca barajar";
                limpiarlog();
                break;
            }
            break;
        }
        document.getElementById("aceptarboton").style.left=igual;
    }
}

function envida2Envido (){
    if (envidoJuego == false && vozenvido == true && florJuego == false){
        if (flor)
        envidoJuego = true;
        puntosEnvido += 2;
        vozenvido == false;
        responderBotenvido ();
        logEnvido ();
    }
}

function envida4Envido (){
    if (envidoJuego == false && vozenvido == true && florJuego == false){
        envidoJuego = true;
        puntosEnvido += 4;
        vozenvido == false;
        responderBotenvido ();
        logEnvido ();
    }
}

function envida8Envido (){
    if (envidoJuego == false && vozenvido == true && florJuego == false){
        envidoJuego = true;
        puntosEnvido += 8;
        vozenvido == false;
        responderBotenvido ();
        logEnvido ();
    }
}

function faltaEnvido (){
    if (envidoJuego == false && vozenvido == true && florJuego == false){
        envidoJuego = true;
        if (piedrasJugador>=piedrasBot)resto = piedrasJugador - 18;
        else resto = piedrasBot - 18;
        puntosEnvido += resto;
        vozenvido == false;
        responderBotenvido ();
        logEnvido ();
    }
}

function logFlor (){
    if (cantoflorbot == true && cartamesaBot1 == -1){
        document.getElementById("flor").innerHTML="Bot: Flor";
    }
    if (cantoflorjug == true && cartamesaJugador1 == -1){
        document.getElementById("flor").innerHTML="Jugador: Flor";
    }
    if (florchicabot == true){
        document.getElementById("flor").innerHTML="Bot: Flor chica";
    }
    if (florchicajug == true){
        document.getElementById("flor").innerHTML="Jugador: Flor chica";
    }
    if (botquisoflor == true){
        document.getElementById("flor").innerHTML="El bot quiso el envido de flor";
    }
    if (botquisoflor == false && florJuego == true && cantoflorbot == true) {
        document.getElementById("flor").innerHTML="El bot no quiso el envido de flor";
        florJuego = false;
        piedrasJugador += 3;
        document.getElementById("piedrasjug").innerHTML=`Piedras del jugador: ${piedrasJugador}`;
    }
}


function cantoFlor(){
    if (florJuego == false && vozflor == true){
        if (florJugador>0) {
            vozflor = false;
            florJuego = true;
            puntosFlor += 3;
            envidoJuego = false;
            cantoflorjug = true;
            logFlor ();
        }
    }
}

function chicaflor (){
    if (florJuego == true && cantoflorjug == true && cantoflorbot == true && vozenvidoflor == true){
        florchicajug= true;
        vozenvidoflor = false;
        desicionFlorbot();
        logFlor();
    }
}

function envida2Flor(){
    if (florJuego == true && cantoflorjug == true && cantoflorbot == true && vozenvidoflor == true){
        florchicajug= false;
        vozenvidoflor = false;
        puntosFlor +=2;
        desicionFlorbot();
        logFlor();
    }
}

function envida4Flor(){
    if (florJuego == true && cantoflorjug == true && cantoflorbot == true && vozenvidoflor == true){
        florchicajug= false;
        vozenvidoflor = false;
        puntosFlor +=4;
        desicionFlorbot();
        logFlor();
    }
}

function envida8Flor(){
    if (florJuego == true && cantoflorjug == true && cantoflorbot == true && vozenvidoflor == true){
        florchicajug= false;
        vozenvidoflor = false;
        puntosFlor +=8;
        desicionFlorbot();
        logFlor();
    }
}

function faltaFlor(){
    if (florJuego == true && cantoflorjug == true && cantoflorbot == true && vozenvidoflor == true){
        florchicajug= false;
        vozenvidoflor = false;
        if (piedrasJugador>=piedrasBot)resto = piedrasJugador - 18;
        else resto = piedrasBot - 18;
        puntosFlor += resto;
        desicionFlorbot();
        logFlor();
    }
}

function evaluarPuntosflor (){
    if (cantoflorbot==true && cantoflorjug==true && florJuego == true){
        if (florJugador>florBot ){
            piedrasJugador += puntosFlor;
            ganadorFlor = 1;
        }
        if (florJugador<florBot && florJuego == true){
            piedrasBot += puntosFlor;
            ganadorFlor = 2;
        }
        else {
            if (turnoJugar==0 && florJuego == true){
                piedrasJugador += puntosFlor;
                ganadorFlor = 3;
            }
            if (turnoJugar==1 && florJuego == true){
                piedrasBot += puntosFlor;
                ganadorFlor = 4;
            }
        }
    }
    if (cantoflorbot == false && cantoflorjug == true  && florJuego == true){
        piedrasBot += puntosFlor;
        ganadorFlor=6;
    }
    if (cantoflorbot == true && cantoflorjug == false  && florJuego == true){
        piedrasBot += puntosFlor;
        ganadorFlor=6;
    }
}

function logEnvido (){
    if (botquisoenvido== true){
        document.getElementById("envido").innerHTML="El bot quiso el envido";
    }
    else {
        document.getElementById("envido").innerHTML="El bot no quiso el envido";
        envidoJuego = false;
        if (florJuego==false){
            piedrasJugador += 1;
            document.getElementById("piedrasjug").innerHTML=`Piedras del jugador: ${piedrasJugador}`;
        }
    }
}
function botontruco (){
    if (voz == true && truco==false) {
        truco = true;
        responderBottruco();
        if (botquiso== true){
            document.getElementById("truco").innerHTML="El bot quiso el truco";
            voz= false;
        }
        else {
            truco= false;
            findejuego = true;
            document.getElementById("truco").innerHTML="El bot no quiso el truco";
            esperandoRespuesta= false;
            ganadorTruco=1;
            mostrarLog();
        }
    }
}
function botonretruco (){
    if (voz == true && truco==true) {
        truco = false;
        retruco = true;
        responderBottruco();
        if (botquiso== true){
            document.getElementById("truco").innerHTML="El bot quiso el truco";
            voz= false;
        }
        else {
            truco = true;
            retruco= false;
            findejuego = true;
            document.getElementById("truco").innerHTML="El bot no quiso el truco";
            esperandoRespuesta= false;
            ganadorTruco=1;
            mostrarLog();
        }
    }
}
function botonvale9 (){
    if (voz == true && retruco == true) {
        retruco = false;
        valenueve = true;
        responderBottruco();
        if (botquiso== true){
            document.getElementById("truco").innerHTML="El bot quiso el truco";
            voz= false;
        }
        else {
            retruco = true;
            valeneve= false;
            findejuego = true;
            document.getElementById("truco").innerHTML="El bot no quiso el truco";
            esperandoRespuesta= false;
            ganadorTruco=1;
            mostrarLog();
        }
    }
}
function botonvalejuego(){
    if (voz == true && valenueve == true) {
        valenueve = false;
        valejuego = true;
        responderBottruco();
        if (botquiso== true){
            document.getElementById("truco").innerHTML="El bot quiso el truco";
            voz= false;
        }
        else {
            valenueve= true;
            valejuego= false;
            findejuego = true;
            document.getElementById("truco").innerHTML="El bot no quiso el truco";
            esperandoRespuesta= false;
            ganadorTruco=1;
            mostrarLog();
        }
    }
}


function evaluarPuntostruco () {
    if (truco == true){
        if (ganadorTruco == 1)piedrasJugador += 3;
        if (ganadorTruco == 2)piedrasBot += 3;
    }
    if (retruco == true){
        if (ganadorTruco == 1)piedrasJugador += 6;
        if (ganadorTruco == 2)piedrasBot += 6;
    }
    if (valenueve == true){
        if (ganadorTruco == 1)piedrasJugador += 9;
        if (ganadorTruco == 2)piedrasBot += 9;
    }
    if (valejuego == true){
        if (ganadorTruco == 1)piedrasJugador += 18;
        if (ganadorTruco == 2)piedrasBot += 18;
    }
    if (truco == false && retruco == false  && valenueve == false && valejuego == false) {
        if (ganadorTruco == 1)piedrasJugador += 1;
        if (ganadorTruco == 2)piedrasBot += 1;
    }
}
function evaluarPuntosenvido (){
    if (envidoJuego==true){
        if (envidoJugador>envidoBot){
            piedrasJugador += puntosEnvido;
            ganadorEnvido = 1;
        }
        if (envidoJugador<envidoBot){
            piedrasBot += puntosEnvido;
            ganadorEnvido = 2;
        }
        else {
            if (turnoJugar==0){
                piedrasJugador += puntosEnvido;
                ganadorEnvido = 3;
            }
            if (turnoJugar==1){
                piedrasBot += puntosEnvido;
                ganadorEnvido = 4;
            }
            
        }
    }
}

function evaluarEmpardo (){
    if (cartamesaJugador1 != -1 && cartamesaBot1 != -1  && valorCartas[cartamesaJugador1] == valorCartas[cartamesaBot1]){
        empardo = true;
    }
}
function limpiarMesa (){
    limpiarlog();
    cartamesaJugador1 = -1;
    cartamesaJugador2 = -1;
    cartamesaJugador3 = -1;
    cartamesaBot1 = -1;
    cartamesaBot2 = -1;
    cartamesaBot3 = -1;
    document.getElementById("carta1").src = "imagenes/fondomano.png";
    document.getElementById("carta2").src = "imagenes/fondomano.png";
    document.getElementById("carta3").src = "imagenes/fondomano.png";
    document.getElementById("carta4").src = "imagenes/fondomano.png";
    document.getElementById("carta5").src = "imagenes/fondomano.png";
    document.getElementById("carta6").src = "imagenes/fondomano.png";
    document.getElementById("cartamano1").src = "imagenes/fondomano.png";
    document.getElementById("cartamano2").src = "imagenes/fondomano.png";
    document.getElementById("cartamano3").src = "imagenes/fondomano.png";
    document.getElementById("vira").src = "imagenes/fondomano.png";
    truco = false;
    retruco = false;
    valenueve = false;
    valejuego = false;
    envidoJuego = false;
    puntosEnvido = 0;
    florJuego = false;
    puntosFlor = 0;
    botquiso = false;
    botquisoenvido = false;
    ganadorEnvido = 0;
    ganadorFlor = 0;
    vozenvidoflor = false;
    florchicajug = false;

}

/*  PANTALLA DE CARGA  */
function PantallaCarga() {
    var contenedor = document.getElementById('contenedor_carga');
    contenedor.style.visibility = 'hidden';
    contenedor.style.opacity = '0';
    jugarTruco = true;
}
