function desicionBotcarta1 (){
    //modo normal tirando la primera media y dejando mas si es mano, y si no es y puede matar primera lo mata
    while (2>1){
        // si tiene flor reservada 
        if (pericoCartas[cartabot1] == 2 || pericoCartas[cartabot2] == 2 || pericoCartas[cartabot3] == 2 ) { 
            if (pericoCartas[cartabot1] == 1 || pericoCartas[cartabot2] == 1 || pericoCartas[cartabot3] == 1) {
                if (pericoCartas[cartabot1] == 0){
                    ponerCartabot1mesa1();
                    break;
                }
                if (pericoCartas[cartabot2] == 0){
                    ponerCartabot2mesa1();
                    break;
                }
                if (pericoCartas[cartabot3] == 0){
                    ponerCartabot3mesa1();
                    break;
                }
            }
        }
        // si el bot juega segundo
        if (turnoJugar==0){
            // si tiene perico, trata de matar primera con la mas baja o emparda, guardando perico, si no puede matar el 
            // se deja matar
            if (pericoCartas[cartabot1] == 2 || pericoCartas[cartabot2] == 2 || pericoCartas[cartabot3] == 2 ){                
                // si la primera carta del bot es perico
                if (pericoCartas[cartabot1] == 2){
                    if (valorCartas[cartabot2] >= valorCartas[cartabot3] ){
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        else {
                            ponerCartabot3mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        else {
                            ponerCartabot2mesa1();
                            break;
                        }
                    }
                }
                //si la segunda carta del bot es perico
                if (pericoCartas[cartabot2] == 2){
                    if (valorCartas[cartabot1] >= valorCartas[cartabot3] ){
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        else {
                            ponerCartabot3mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        else {
                            ponerCartabot1mesa1();
                            break;
                        }
                    }
                }
                //si la tercera carta del bot es perico
                if (pericoCartas[cartabot3] == 2){
                    if (valorCartas[cartabot1] >= valorCartas[cartabot2] ){
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        else {
                            ponerCartabot2mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        else {
                            ponerCartabot1mesa1();
                            break;
                        }
                    }
                }
            }
            // si tiene perica, trata de matar primera con la mas baja o emparda, guardando perica, si no puede matar el 
            // se deja matar
            if (pericoCartas[cartabot1] == 1 || pericoCartas[cartabot2] == 1 || pericoCartas[cartabot3] == 1 ){                
                // si la primera carta del bot es perica
                if (pericoCartas[cartabot1] == 1){
                    if (valorCartas[cartabot2] >= valorCartas[cartabot3] ){
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        else {
                            ponerCartabot3mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        else {
                            ponerCartabot2mesa1();
                            break;
                        }
                    }
                }
                //si la segunda carta del bot es perica
                if (pericoCartas[cartabot2] == 1){
                    if (valorCartas[cartabot1] >= valorCartas[cartabot3] ){
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        else {
                            ponerCartabot3mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        if (valorCartas[cartabot3]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot3mesa1();
                            break;
                        }
                        else {
                            ponerCartabot1mesa1();
                            break;
                        }
                    }
                }
                //si la tercera carta del bot es perica
                if (pericoCartas[cartabot3] == 1){
                    if (valorCartas[cartabot1] >= valorCartas[cartabot2] ){
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        else {
                            ponerCartabot2mesa1();
                            break;
                        }
                    }
                    else {
                        if (valorCartas[cartabot1]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot1mesa1();
                            break;
                        }
                        if (valorCartas[cartabot2]>= valorCartas[cartamesaJugador1]){
                            ponerCartabot2mesa1();
                            break;
                        }
                        else {
                            ponerCartabot1mesa1();
                            break;
                        }
                    }
                }
            }
            // si no hay perico o perica, lanza la carta mas baja q mate la primera, si no lanza la segunda, si no la tercera
            // si ninguna mata lanza la mas baja
            // si la primera es la mas alta de todas
            if (valorCartas[cartabot1] >= valorCartas[cartabot2] && valorCartas[cartabot1] >= valorCartas[cartabot3]){
                if (valorCartas[cartabot2] >= valorCartas[cartabot3] ){
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot2mesa1();
                        break;
                    }
                }
            }
            // si la segunda es la mas alta de todas
            if (valorCartas[cartabot2] >= valorCartas[cartabot1] && valorCartas[cartabot2] >= valorCartas[cartabot3]){
                if (valorCartas[cartabot1] >= valorCartas[cartabot3] ){
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    else {
                        ponerCartabot1mesa1();
                        break;
                    }
                }
            }
            // si la tercera es la mas alta de todas
            if (valorCartas[cartabot3] >= valorCartas[cartabot1] && valorCartas[cartabot3] >= valorCartas[cartabot2]){
                if (valorCartas[cartabot1] >= valorCartas[cartabot2] ){
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    else {
                        ponerCartabot2mesa1();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot1]> valorCartas[cartamesaJugador1]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    if (valorCartas[cartabot2]> valorCartas[cartamesaJugador1]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    if (valorCartas[cartabot3]> valorCartas[cartamesaJugador1]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    else {
                        ponerCartabot1mesa1();
                        break;
                    }
                }
            }            
        }
        if (turnoJugar == 1){
            if (pericoCartas[cartabot1] == 2 || pericoCartas[cartabot2] == 2 || pericoCartas[cartabot3] == 2 || 
            pericoCartas[cartabot1] == 1 || pericoCartas[cartabot2] == 1 || pericoCartas[cartabot3] == 1){
                if (pericoCartas[cartabot1] != 0 ){
                    if (valorCartas[cartabot2]>= valorCartas[cartabot3]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                if (pericoCartas[cartabot2] != 0 ){
                    if (valorCartas[cartabot1]>= valorCartas[cartabot3]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                if (pericoCartas[cartabot3] != 0 ){
                    if (valorCartas[cartabot1]>= valorCartas[cartabot2]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot2mesa1();
                        break;
                    }
                }

            }
            picardia = Math.round(Math.random()*6);
            if (picardia == 0 || picardia == 2 || picardia == 5){
                if (valorCartas[cartabot1]>=valorCartas[cartabot2] && valorCartas[cartabot1]>=valorCartas[cartabot3]){
                    if (valorCartas[cartabot2]>=valorCartas[cartabot3]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    else {
                        ponerCartabot2mesa1();
                        break;
                    }
                }
                if (valorCartas[cartabot2]>=valorCartas[cartabot1] && valorCartas[cartabot2]>=valorCartas[cartabot3]){
                    if (valorCartas[cartabot1]>=valorCartas[cartabot3]){
                        ponerCartabot3mesa1();
                        break;
                    }
                    else {
                        ponerCartabot1mesa1();
                        break;
                    }
                }
                if (valorCartas[cartabot3]>=valorCartas[cartabot1] && valorCartas[cartabot3]>=valorCartas[cartabot2]){
                    if (valorCartas[cartabot1]>=valorCartas[cartabot2]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    else {
                        ponerCartabot1mesa1();
                        break;
                    }
                }
            }
            if (picardia == 1 || picardia == 4 || picardia == 6){
                if (valorCartas[cartabot1]>=valorCartas[cartabot2] && valorCartas[cartabot1]>=valorCartas[cartabot3]){
                    if (valorCartas[cartabot2]>=valorCartas[cartabot3]){
                        ponerCartabot2mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                if (valorCartas[cartabot2]>=valorCartas[cartabot1] && valorCartas[cartabot2]>=valorCartas[cartabot3]){
                    if (valorCartas[cartabot1]>=valorCartas[cartabot3]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot3mesa1();
                        break;
                    }
                }
                if (valorCartas[cartabot3]>=valorCartas[cartabot1] && valorCartas[cartabot3]>=valorCartas[cartabot2]){
                    if (valorCartas[cartabot1]>=valorCartas[cartabot2]){
                        ponerCartabot1mesa1();
                        break;
                    }
                    else {
                        ponerCartabot2mesa1();
                        break;
                    }
                }
            }
            if (picardia == 3){
                if (valorCartas[cartabot1]>=valorCartas[cartabot2] && valorCartas[cartabot1]>=valorCartas[cartabot3]){
                    ponerCartabot1mesa1();
                    break;
                }
                if (valorCartas[cartabot2]>=valorCartas[cartabot1] && valorCartas[cartabot2]>=valorCartas[cartabot3]){
                    ponerCartabot2mesa1();
                    break;
                }
                if (valorCartas[cartabot3]>=valorCartas[cartabot1] && valorCartas[cartabot3]>=valorCartas[cartabot2]){
                    ponerCartabot3mesa1();
                    break;
                }
            }

        }
        console.log("El bot no respondio");
        break;
    }
    console.log(`${picardia}`);
}

function desisionBotcarta2 (){
    while (2>1){
        if (valorCartas[cartamesaJugador1]<valorCartas[cartamesaBot1]){
            if (cartabot1==-1){
                if (valorCartas[cartabot2]>= valorCartas[cartabot3]){
                    ponerCartabot3mesa2();
                    break;
                }
                else {
                    ponerCartabot2mesa2();
                    break;
                }
            }
            if (cartabot2==-1){
                if (valorCartas[cartabot1]>= valorCartas[cartabot3]){
                    ponerCartabot3mesa2();
                    break;
                }
                else {
                    ponerCartabot1mesa2();
                    break;
                }
            }
            if (cartabot3==-1){
                if (valorCartas[cartabot1]>= valorCartas[cartabot2]){
                    ponerCartabot2mesa2();
                    break;
                }
                else {
                    ponerCartabot1mesa2();
                    break;
                }
            }
        }
        if (valorCartas[cartamesaJugador1]>valorCartas[cartamesaBot1]){
            if (cartabot1==-1){
                if (valorCartas[cartabot2]>= valorCartas[cartabot3]){
                    if (valorCartas[cartabot3]>valorCartas[cartamesaJugador2]){
                        ponerCartabot3mesa2();
                        break;
                    }
                    if (valorCartas[cartabot2]>valorCartas[cartamesaJugador2]){
                        ponerCartabot2mesa2();
                        break;
                    }
                    else {
                        ponerCartabot3mesa2();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot2]>valorCartas[cartamesaJugador2]){
                        ponerCartabot2mesa2();
                        break;
                    }
                    if (valorCartas[cartabot2]>valorCartas[cartamesaJugador2]){
                        ponerCartabot3mesa2();
                        break;
                    }
                    else {
                        ponerCartabot2mesa2();
                        break;
                    }
                }
            }
            if (cartabot2==-1){
                if (valorCartas[cartabot1]>= valorCartas[cartabot3]){
                    if (valorCartas[cartabot3]>valorCartas[cartamesaJugador2]){
                        ponerCartabot3mesa2();
                        break;
                    }
                    if (valorCartas[cartabot1]>valorCartas[cartamesaJugador2]){
                        ponerCartabot1mesa2();
                        break;
                    }
                    else {
                        ponerCartabot3mesa2();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot3]>valorCartas[cartamesaJugador2]){
                        ponerCartabot1mesa2();
                        break;
                    }
                    if (valorCartas[cartabot1]>valorCartas[cartamesaJugador2]){
                        ponerCartabot3mesa2();
                        break;
                    }
                    else {
                        ponerCartabot1mesa2();
                        break;
                    }
                }
            }
            if (cartabot3==-1){
                if (valorCartas[cartabot1]>= valorCartas[cartabot2]){
                    if (valorCartas[cartabot2]>valorCartas[cartamesaJugador2]){
                        ponerCartabot2mesa2();
                        break;
                    }
                    if (valorCartas[cartabot1]>valorCartas[cartamesaJugador2]){
                        ponerCartabot1mesa2();
                        break;
                    }
                    else {
                        ponerCartabot2mesa2();
                        break;
                    }
                }
                else {
                    if (valorCartas[cartabot1]>valorCartas[cartamesaJugador2]){
                        ponerCartabot1mesa2();
                        break;
                    }
                    if (valorCartas[cartabot2]>valorCartas[cartamesaJugador2]){
                        ponerCartabot2mesa2();
                        break;
                    }
                    else {
                        ponerCartabot1mesa2();
                        break;
                    }
                }
            }
        }
        console.log("El bot no consiguio respuesta a la segunda carta");
        break;
    }
}

function desisionBotcarta3 (){
    while (2>1){
        if (cartabot1 == -1 && cartabot2 == -1){
            ponerCartabot3mesa3();
            break;
        }
        if (cartabot1 == -1 && cartabot3 == -1){
            ponerCartabot2mesa3();
            break;
        }
        if (cartabot3 == -1 && cartabot2 == -1){
            ponerCartabot1mesa3();
            break;
        }
        console.log ("El bot no consiguio respuesta a la tercera carta");
        break;
    }
}


function desisionBotempardo (){
    while(2>1){
        if (cartabot1==-1){
            if (valorCartas[cartabot2]>= valorCartas[cartabot3]){
                ponerCartabot2mesa3();
                cartamesaBot2 = cartabot3;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
            else {
                ponerCartabot3mesa3();
                cartamesaBot2 = cartabot2;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
        }
        if (cartabot2==-1){
            if (valorCartas[cartabot1]>= valorCartas[cartabot3]){
                ponerCartabot1mesa3();
                cartamesaBot2 = cartabot3;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
            else {
                ponerCartabot3mesa3();
                cartamesaBot2 = cartabot1;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
        }
        if (cartabot3==-1){
            if (valorCartas[cartabot1]>= valorCartas[cartabot2]){
                ponerCartabot1mesa3();
                cartamesaBot2 = cartabot2;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
            else {
                ponerCartabot2mesa3();
                cartamesaBot2 = cartabot1;
                document.getElementById("carta5").src ="imagenes/naipe.png";
                break;
            }
        }
        console.log("El bot no consiguio respuesta al empardo");
        break;
    }   
}

function responderBottruco (){
    if (truco == true){
        if (valorcartasBot>=24)botquiso=true;
    }
    if (retruco == true){
        if (valorcartasBot>=28)botquiso=true;
    }
    if (valenueve == true){
        if (valorcartasBot>=31)botquiso=true;
    }
    if (valejuego == true){
        if (valorcartasBot>32)botquiso=true;
    }
}

function responderBotenvido (){
    if (florBot == 0 && envidoJuego== true){
        if (envidoBot >= 34 ){
            botquisoenvido = true;
        }
        if (puntosEnvido >= 5 && puntosEnvido <=7  ){
            if (envidoBot <= 33 && envidoBot >= 31 && botquisoenvido == false){
                botquisoenvido = true;
            }
        }
        if ( puntosEnvido == 3 || puntosEnvido == 4 ){
            if (envidoBot <= 30 && envidoBot == 29 && botquisoenvido == false){
                botquisoenvido = true;
            }
        }
        if ( puntosEnvido == 2 ){
            if (envidoBot <= 28 && envidoBot >= 25 && botquisoenvido == false){
                botquisoenvido = true;
            }
        }
    }
    if (florBot > 0 && florJuego == false && cantoflorbot == false && cartamesaBot1 == -1){
        cantoflorbot = true;
        florJuego = true;
        logFlor ();
    }
}

function desicionFlorbot (){
    if (florBot > 0 && cartamesaBot1 == -1 && cantoflorbot == false){
        cantoflorbot = true;
        florJuego = true;
        logFlor();
    }
    if (cantoflorbot == true && cantoflorjug == true && florchicajug == true){
        florchicabot = true;
        logFlor();
    }
    if (cantoflorbot == true && cantoflorjug == true && florchicajug == false && puntosFlor == 3){
        florchicabot = true;
        logFlor();
    }
    if (cantoflorbot == true && cantoflorjug == true && florchicajug == false && puntosFlor > 3 && puntosFlor <= 5){
        if (florBot>30){
            botquisoflor == true;
            florchicabot == false;
            logFlor();
        }
    }
    if (cantoflorbot == true && cantoflorjug == true && florchicajug == false && puntosFlor > 3 && puntosFlor <= 5){
        if (florBot>30){
            botquisoflor == true;
            florchicabot == false;
            logFlor();
        }
    }
}

function ponerCartabot1mesa1(){
    cartamesaBot1= cartabot1;
    document.getElementById("carta4").src = ubicacionCartas[cartabot1];
    cartabot1 = -1;
}
function ponerCartabot2mesa1(){
    cartamesaBot1= cartabot2;
    document.getElementById("carta4").src = ubicacionCartas[cartabot2];
    cartabot2 = -1;
}
function ponerCartabot3mesa1(){
    cartamesaBot1= cartabot3;
    document.getElementById("carta4").src = ubicacionCartas[cartabot3];
    cartabot3 = -1;
}
function ponerCartabot1mesa2(){
    cartamesaBot2= cartabot1;
    document.getElementById("carta5").src = ubicacionCartas[cartabot1];
    cartabot1 = -1;
}
function ponerCartabot2mesa2(){
    cartamesaBot2= cartabot2;
    document.getElementById("carta5").src = ubicacionCartas[cartabot2];
    cartabot2 = -1;
}
function ponerCartabot3mesa2(){
    cartamesaBot2= cartabot3;
    document.getElementById("carta5").src = ubicacionCartas[cartabot3];
    cartabot3 = -1;
}
function ponerCartabot1mesa3(){
    cartamesaBot3= cartabot1;
    document.getElementById("carta6").src = ubicacionCartas[cartabot1];
}
function ponerCartabot2mesa3(){
    cartamesaBot3= cartabot2;
    document.getElementById("carta6").src = ubicacionCartas[cartabot2];
}
function ponerCartabot3mesa3(){
    cartamesaBot3= cartabot3;
    document.getElementById("carta6").src = ubicacionCartas[cartabot3];
}
